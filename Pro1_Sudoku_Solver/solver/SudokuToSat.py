#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 

# SudokuToSat: convert from Sudoku problem in a raw input file into DIMACS format (input for miniSat solver)
from pprint import pprint
import math

size     = None # global variable for size of sudoku = subsize * subsize
subsize  = None # global variable for size of sub-grid sudoku
def read_file(sodukufile):
    """
    Read file Sudoku problem in raw (grid) input file into a two dimensions array
    """
    global size     # access global variable
    global subsize
    size     = None # reset size to zero
    subsize  = None
    datapath = "../data/" + sodukufile
    try:
        rawinput = open(datapath, "r")
        grid = []
        for line in rawinput:
            if size == None:
                size = int((line.split())[0]) # split() returns a list of all the words in the string
                subsize = int(math.sqrt(size))
                continue
            grid.append([int(x) for x in line.split()])
        bsize = len(grid)
        
        # test grid size is square ?
        if (size != bsize):
            print "The input file is in incorrect format. Please check again!"
            return -1
        for row in grid:
            if(len(row) != bsize):
                print "The input file is in incorrect format. Please check again." 
                return -1
        #print size; pprint(grid)
        return grid
    except IOError:
        print "The file does not exist."
        return -1
    except ValueError:
        print "There are non-number symbols in your file. Please check again."
        return -1
    
def index(x, y, z):
    """
    Return the index of variable (in array) of cell at row x, column y and assigned number z.
    The range: 1 ~ n^3 propositional variable, x = (1,n), y = (1,n), z = (1,n)
    Formula index of cell = n*n*(i-1) + n*(y-1) + z (with n is size of grid in one dimension)
    """
    # return 81*(x-1) + 9*(y-1) + z # n = 9
    return size*size*(x-1) + size*(y-1) + z

def sudoku_constraint():
    """
    Create the Sudoku clauses from sudoku constraint and minimal encoding method
    Then, return them as list of clauses.
    Note: these sudoku clause are independent of the particular Sudoku puzzle (same in all Sudoku)
    """
    clauses = []
    
    # At least one number in each cell - size*size size-ary clauses
    for x in range(1, size+1): # range([start], [stop]) means start <= x < stop
        for y in range(1, size+1):
            # size literal each clauses
            clauses.append([index(x, y, z) for z in range(1, size+1)])
    #print len(clauses)
    
    # Each number appears at most once in each column - size*size*C(2,size) binary clauses
    for y in range(1, size+1):
        for z in range(1, size+1):
            for x in range (1, size):
                for i in range (x+1, size+1):
                    clauses.append([-index(x, y, z), -index(i, y, z)])
    #print len(clauses)
    
    # Each number appears at most once in each row - size*size*C(2,size) binary clauses
    for x in range(1, size+1):
        for z in range(1, size+1):
            for y in range (1, size):
                for i in range (y+1, size+1):
                    clauses.append([-index(x, y, z), -index(x, i, z)])
    #print len(clauses)
    
    # Each number appears at most once in each 3*3 sub-grid - size*size*C(2,size) binary clauses
    for z in range(1, size+1):
        for i in range(0, subsize):
            for j in range (0, subsize):
                for x in range (1, subsize+1):
                    for y in range(1, subsize+1):
                        for k in range(y+1, subsize+1):
                            clauses.append([-index(subsize*i+x, subsize*j+y, z), -index(subsize*i+x, subsize*j+k, z)])
    for z in range(1, size+1):
        for i in range(0, subsize):
            for j in range (0, subsize):
                for x in range (1, subsize+1):
                    for y in range(1, subsize+1):
                        for k in range(x+1, subsize+1):
                            for l in range(1, subsize+1):
                                clauses.append([-index(subsize*i+x, subsize*j+y, z), -index(subsize*i+k, subsize*j+l, z)])
    
    #pprint(clauses)
    #print len(clauses)
    assert len(clauses) == size*size + (3*size*size*math.factorial(size)/math.factorial(size-2))/2
    return clauses

def part_constraint(gridInput):
    """
    Create the clauses from particular Sudoku puzzle.
    Each pre-assign variable is a clause (with one literal).
    """
    clauses = []
    for x in range(1, size+1):
        for y in range(1, size+1):
            z = gridInput[x - 1][y - 1] # index in Python from 0
            # Note: Improve efficient: remove all variables for the known cells
            #     However, the code will become more complex
            if z:
                clauses.append([index(x, y, z)])
                #print str(x) + str(y) + str(z)
    return clauses

def write_grid_to_cnfinput(gridInput, inputFile):
    """
    Write lists clause to DIMACS format (cnf format input for SAT solver
    """
    clauses1 = sudoku_constraint()
    clauses2 = part_constraint(gridInput)
    
    fo = file("../data/" + inputFile + ".cnfinput",'w')
    fo.write('c Sudoku problem\n')
    fo.write('p cnf %d %d\n' % (size**3, len(clauses1) + len(clauses2)))
    
    fo.write('\nc Sudoku constraints\n')
    for clause in clauses1:
        for literal in clause:
            fo.write(str(literal) + ' ')
        fo.write('0\n')
    fo.write('\nc Input constraints\n')
    for clause in clauses2:
        for literal in clause:
            fo.write(str(literal) + ' ')
        fo.write('0\n')
    
    fo.close()

def main(inputFile):
    gridInput = read_file(inputFile)
    if (gridInput != -1): # read file success
        print "Soduku problem \n"
        write_grid_to_cnfinput(gridInput, inputFile)
        
        # print to screen input sudoku
        width = 1 # width of a number in print screen
        if size > 9: width = 2
        formatscreen = '%' + str(width) + 's' # '%2s'
        for row in gridInput:
            for cell in row:
                print (formatscreen % str(cell)), # do not break new line
                #print str(cell),
            print ""
        return size
        print gridInput
#main("sudoku02")
#main("sudoku04")