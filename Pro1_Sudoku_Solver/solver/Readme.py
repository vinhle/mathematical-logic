# Print introduction about the program
def introduction():
    print """    Wellcome to JAIST Sudoku solver!
    Author: Le Vinh (1410212) - JAIST, 5/2015
    - The Sudoku solver is a program with command GUI style and it cooperates with SAT solver (miniSAT)
    - The Sudoku solver can solve square sudoku in many sizes (4*4, 9*9, 16*16, ...)
    - The program provides some functions, and please choose functions you like
        1. Try with default sample Sudoku02
        2. Run with your input file (you also can run with other sample: sudoku02, sudoku03, ...)
        3. Help - Explain about the program.
        4. Exit program"""

# Print explanation about the program
def helpsolver():
    print """    1. Introduction about Sudoku Puzzle game: http://en.wikipedia.org/wiki/Sudoku
    2. File format convention
    - The raw input file: First line contains only one number that is the size of Sudoku puzzle
    (Ex: 9 means the size is 9*9). The following lines is grid of square soduku, empty cell is number 0.
    See example files for more detail ...
        + After the solver run, the following files will be generated automatically:
        + <file_name>.cnfinput file: DIMACS format (input file for miniSat solver)
        Read more: http://logic.pdmi.ras.ru/~basolver/dimacs.html
        + <file_name>.cnfoutput file: output file from miniSat solver
        + <file_name>.gridouput (final output): the same format with raw input file (easy to see the solution)
    3. Run the program (In Linux enviroment)
    - Your data must be placed in folder data
    - Your computer must install Python 2.7
    - Open terminal
    - Move to program source: cd Soduku/src/solver
    - Run by command: python ./SudokuSolver.py
    """