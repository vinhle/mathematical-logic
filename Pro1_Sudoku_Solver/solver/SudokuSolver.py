#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 

"""
Soduku Solver - Main program
"""
import Readme
import SudokuToSat
import CallMiniSat
import SatToSudoku

def main():
    Readme.introduction()
    sodukuDefault = "sudoku02"
    size = None # size of sudoku
    while True:
        choice        = raw_input("* Your choice: ")
        
        if choice == "1":
            size = SudokuToSat.main(sodukuDefault)
            CallMiniSat.main(sodukuDefault)
            SatToSudoku.main(sodukuDefault, size)
        else:
            if choice == "2":
                sodukuUser = raw_input("* Your file: ")
                size = SudokuToSat.main(sodukuUser)
                CallMiniSat.main(sodukuUser)
                SatToSudoku.main(sodukuUser, size)
            else:
                if choice == "3": Readme.helpsolver()
                else:
                    if choice == "4": print "Thank you, bye bye"; break
                    else: print "Your choice is incorrect format! Please input again."
        print "Finish"
    
main()