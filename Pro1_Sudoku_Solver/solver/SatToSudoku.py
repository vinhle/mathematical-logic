#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 

from pprint import pprint
import math

# SudokuToSat: convert from DIMACS format to grid ouput of soduku solution
def index(x, y, z):
    """
    Return the number of variable (index in array) of cell at row x, column y and assigned number z.
    The range: 1 ~ 729 propositional variable, x = (1,9), y = (1,9), z = (1,9)
    Formula index of cell = n*n*(i-1) + n*(y-1) + z (with n is size of grid in one dimension)
    """
    # return 81*(x-1) + 9*(y-1) + z # n = 9
    return size*size*(x-1) + size*(y-1) + z

# convert one dimensional array to two dimensional array python 
# convert list l to two-dimension list with n elements in each sub list
def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

def write_cnfoutput_to_grid(inputFile):    
    datapath = "../data/" + inputFile + ".cnfoutput"
    
    # array includes all propositional variable
    array = []
    result = None
    try:
        fi = open(datapath, "r")
        for line in fi:
            if result == None:
                result = line.split()[0] # only get the first element in line
                if result == "UNSAT":
                    print "There is no solution for this Soduku"
                    break
                else: # "SAT"
                    continue
            else:
                #array.append(int(x) for x in line.split())
                for e in line.split():
                    if (e != "0"):
                        array.append(int(e))
        
        # array = sorted(array, key=abs) # sort by absolute value, but Minisat have already sorted value from 1
        assert len(array) == size**3 # test the size        
        #print array
    except IOError:
        print "The file does not exist."
    except AssertionError:
        print "Error size of grid soduku"
    
    # array2 includes all number in soduku solution
    array2 = []
    if result == "SAT":
        for x in range(1, size+1):
            for y in range(1, size+1):
                for z in range(1, size+1):
                    if (array[index(x, y, z)-1] > 0):
                        array2.append(z)
        #pprint(array2)
    
    # Convert one-dimension array to two-dimension array
    grid = chunks(array2, size)        
    print "Soduku solution"
    # print to screen input sudoku
    width = 1 # width of a number in print screen
    if size > 9: width = 2
    formatno = '%' + str(width) + 's'
    for row in grid:
        for cell in row:
            print (formatno % str(cell)),
        print ""
    #pprint(grid)
    
    # Write result to file
    fo = file("../data/" + inputFile + ".gridoutput", "w")
    for row in grid:
        for cell in row:
            #fo.write(str(cell) + ' ')
            fo.write(formatno % str(cell))
            fo.write(' ')
        fo.write('\n')
    fo.close()
    
def main(inputFile, n):
    global size
    global subsize
    size = n
    subsize = int(math.sqrt(size))
    write_cnfoutput_to_grid(inputFile)

size     = None # global variable for size of sudoku = subsize * subsize
subsize  = None # global variable for size of sub-grid sudoku
    