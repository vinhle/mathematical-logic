# Print introduction about the program
def introduction():
    print """    Wellcome to JAIST CNF Conversion!
    Author: Le Vinh (1410212) - JAIST, 5/2015
    - The CNF Conversion is a program with command GUI style
    - The program can convert any your propositional logic formula to conjunctive normal form (cnf)
      by Tseitin Conversion and Distribute method
    - The program provides some functions, and please choose functions you like
        1. Try with default sample formula
        2. Run with your input formula
        3. Help - Explain about the program.
        4. Exit program"""

# Print explanation about the program
def helpsolver():
    print """    1. Introduction
    - CNF form: http://en.wikipedia.org/wiki/Conjunctive_normal_form
    - Tseitin Conversion:
    2. Formula convention
    - Binary operation: and(.), or(+), not(~), imply(->), equivalent(<->)
    - All compound propositions must be bracketed with parenthesis
    Correct examples: (a * b) + c 
                      ~(a -> ~b )
                      ~(~a)
                      a   
    Incorrect examples: a v b v c  , - -a
    3. Run the program (In Linux environment)
    - Your computer must install Python 2.7
    - Open terminal
    - Move to program source: cd CNFConverter/src/
    - Run by command: python ./CNFConversion.py
    """