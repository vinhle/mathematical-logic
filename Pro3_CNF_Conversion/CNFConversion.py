#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 

# Tseitin Conversion - Main program

import Readme
import FormulaParser
import CNFConverter01
import CNFConverter02

def printCNF(cnf):
    cnfformula = "\t"
    len1 = len(cnf)
    for clause in cnf:
        len2 = len(clause)
        cnfformula += "("
        for literal in clause:
            if clause[len2 - 1] == literal: cnfformula += literal
            else: cnfformula = cnfformula + literal + " + "
        if cnf[len1 - 1] == clause: cnfformula += ")"
        else: cnfformula += ") * "
    print cnfformula

def main():
    Readme.introduction()
    
    # ((a+b)*((b+c)+d))+e
    # a+(b*c)
    while True:
        choice = raw_input("* Your choice: ")
        if choice == "1":
            formula = "(a * b) + (c * d)"
            print "* Your propositional formula: " + formula
            ptree = FormulaParser.main(formula)
            cnf = CNFConverter02.main(ptree)
            print "CNF in Tseitin method: %r clauses" % len(cnf); printCNF(cnf)
            cnf = CNFConverter01.main(ptree)
            print "CNF in Distribute method: %r clauses" % len(cnf); printCNF(cnf)
        else:
            if choice == "2":
                formula = raw_input("* Your propositional formula: ")
                ptree = FormulaParser.main(formula)
                cnf = CNFConverter02.main(ptree)
                print "CNF in Tseitin method: %r clauses" % len(cnf); printCNF(cnf)
                cnf = CNFConverter01.main(ptree)
                print "CNF in Distribute method: %r clauses" % len(cnf); printCNF(cnf)
            else:
                if choice == "3": Readme.helpsolver()
                else:
                    if choice == "4": print "Thank you, bye bye"; break
                    else: print "Your choice is incorrect format! Please input again."
        print "Finish"

main()