# Use Tseitin law to convert formula to CNF form
# Operations
AND = "*"
OR  = "+"
NOT = "~"
IMPLY = "->"
EQUIV = "<->"

def eliminateEquivalences(ptree):
    """ Eliminate equivalences: A <-> B  =  (A -> B) * (B -> A)
    Input: old parse tree
    Output: equivalent parse tree (propositional formula), [<->, A, B]  is replaced by  [*, [->, A, B], [->, B, A]
    """
    if isinstance(ptree, str): # proposition is atomic
        result = ptree
    else :  # proposition is compound 
        op = ptree[0]
        if op == NOT:
            tree_tmp = eliminateEquivalences(ptree[1])
            result = [NOT, tree_tmp]
        else : # op  == binary operation
            tree_tmp1 = eliminateEquivalences(ptree[1])
            tree_tmp2 = eliminateEquivalences(ptree[2])
            if op == EQUIV:
                result = [AND, [IMPLY, tree_tmp1, tree_tmp2], [IMPLY, tree_tmp2, tree_tmp1]]
            else :  # op == IMPLY or AND or OR
                result = [op, tree_tmp1, tree_tmp2]
    return result

def eliminateImplications(ptree):
    """ Eliminate implications: A -> B  =  ~A + B
    Input: old parse tree
    Output: equivalent parse tree (propositional formula), [->, A, B]  is replaced by  [+, [~, A], B]
    """
    if isinstance(ptree, str): # proposition is atomic
        result = ptree
    else :  # proposition is compound 
        op = ptree[0]
        if op == NOT:
            tree_tmp = eliminateImplications(ptree[1])
            result = [NOT, tree_tmp]
        else : # op  == binary operation
            tree_tmp1 = eliminateImplications(ptree[1])
            tree_tmp2 = eliminateImplications(ptree[2])
            if op == IMPLY :
                result = [OR, [NOT, tree_tmp1], tree_tmp2]
            else :  # op == AND or OR
                result = [op, tree_tmp1, tree_tmp2]
    return result

def distributeNegations(ptree):
    """ Move negation to front of variable:
    - Double negation elimination: ~(~A) = A
    - Distribute negation - DeMorgan Law: ~(A + B) = ~A * ~B; ~(A * B) = ~A + ~B
    - Input: old parse tree
    - Input: new parse tree
    Replace [~, A] to [~A]; [~, [+, A, B]] to [*, [~,A], [~,B]];  [~, [*, A, B]] to [+, [~,A], [~,B]]"""
    if isinstance(ptree, str): # ptree is atomic proposition
        result = ptree
    else :  # ptree is compound proposition
        op = ptree[0]
        if op == NOT: # ptree = [NOT, arg]
            arg = ptree[1]
            if isinstance(arg, str): # ptree = [NOT, p]
                result = "~" + arg
                #print result
            else:  # ptree = [NOT, [inside_op]]
                in_op = arg[0]
                if in_op == NOT :    # ptree = [NOT, [NOT, ptree']] 
                    result = distributeNegations(arg[1])
                else : # inside operation := AND or OR
                    tree_tmp1 = distributeNegations([NOT, arg[1]])
                    tree_tmp2 = distributeNegations([NOT, arg[2]])
                    # change AND to OR,  OR to AND
                    inverse = {AND: OR, OR: AND}
                    result = [inverse[in_op], tree_tmp1, tree_tmp2] 
        else : # op := AND or OR; ptree = [op, ptree1, ptree2]
            tree_tmp1 = distributeNegations(ptree[1])
            tree_tmp2 = distributeNegations(ptree[2])
            result = [op, tree_tmp1, tree_tmp2]
    return result


num_recs = 0 # the number of recursive steps
count    = 1 # the index number of new variable
cnf = []
def navieTseitin(ptree):
    """Use new variable for every sub formula - much inefficient than distribute law
    This code is only for test and understanding, don't use it
    """
    global num_recs, count, cnf
    num_recs += 1
    #print "lan de quy ", num_recs
    if isinstance(ptree, str) :  # ptree = "P" or "-P" -> return atomic proposition
        result = ptree
    else: # ptree = [op, prop1, prop2] -> return new variable
        op = ptree[0]
        ptree1 = navieTseitin(ptree[1])
        ptree2 = navieTseitin(ptree[2])
        nvar = "x" + str(count) # new variable in string
        if op == AND:
            # h <-> (a+b) := (h + ~a) * (h + ~b) * (~h + a + b)
            #print "hello: " + str(ptree1) + str(ptree2)
            cnf = cnf + [['~'+nvar, ptree1]] + [['~'+nvar, ptree2]] + [[nvar, '~'+ptree1, '~'+ptree2]] 
        else : # op == OR
            # h <-> (a*b) := (h + ~a) * (h + ~b) * (h + ~a + ~b) 
            cnf = cnf + [[nvar, '~'+ptree1]] + [[nvar, '~'+ptree2]] + [['~'+nvar, ptree1, ptree2]]
        count += 1
        result = nvar
    return result

nvar_count = 0 #the number of new variables
def tseitin(ptree):
    global num_recs, nvar_count
    num_recs += 1
    #print "lan de quy ", num_recs
    if isinstance(ptree, str) :  # ptree = "P" or "-P" -> return atomic proposition
        result = ptree
    else: # ptree = [op, prop1, prop2] -> return new variable
        op = ptree[0]
        if op == AND:
            result = [AND, tseitin(ptree[1]), tseitin(ptree[2])]
        if op == OR:
            if isinstance(ptree[2], str):
                result = distributeLaw(tseitin(ptree[1]), ptree[2])
            else: # isinstance(ptree[02], list)
                if ptree[2][0] == AND: # a+(b*c) = (a+x) * (x -> (b*c)) * ((b*c)->x) = (a+x) * (~x + (b*c)) * (~(b*c)+x)
                    nvar_count += 1  # increase recursive variable
                    
                    nvar = "p" + str(nvar_count) # new variable in string
                    
                    #tmp1 = [OR, tseitin2(ptree[1]), 'p'] #neu a la mot bieu thuc cnf thi se sai, nen day phai dung distribute
                    # Fix new var name: tmp1 = distributeLaw(tseitin(ptree[1]), 'p')
                    tmp1 = distributeLaw(tseitin(ptree[1]), nvar)
                    #print "ptree[1]: ", ptree[1]
                    #print "menh de: ", tseitin(ptree[1])
                    #print "tmp1: ", tmp1
                    # Fix new var name: tmp2 = distributeLaw('~p', ptree[2])
                    
                    tmp2 = distributeLaw('~'+nvar, ptree[2])
                    #print "tmp2: ", tmp2
                    tmp3 = [AND, tmp1, tmp2]
                    #print "tmp3: ", tmp3
                    tmp4 = distributeNegations(['~', ptree[2]])
                    # Fix new var name: tmp5 = distributeLaw(tmp4, 'p')
                    tmp5 = distributeLaw(tmp4, nvar)
                    #print "tmp5: ", tmp5
                    
                    result = [AND, tmp3, tmp5] 
                    #print "result: ", result
                    
                    nvar_count -= 1 # descrease recursive variable
                else: # ptree[2][0] == OR
                    # a+(b+c)
                    result = [OR, tseitin(ptree[1]), tseitin(ptree[2])]                    
    return result

def distributeLaw(p1, p2):
    """ Use distribute law to converts to CNF: (a * b) + c = (a+c) * (b+c)
    Input: old parse tree
    Output: new equivalent parse tree in CNF """
    if isinstance(p1, list) and p1[0] == AND:    # p1 = P11 & P12
        result = [AND, distributeLaw(p1[1],p2), distributeLaw(p1[2],p2)]
    elif  isinstance(p2, list) and p2[0] == AND: # p2 = P21 & P22
        result = [AND, distributeLaw(p1,p2[1]), distributeLaw(p1,p2[2])]
    else :
        result = [OR, p1, p2]
    return result

def simplifyCNF(ptree) :
    """ Implicit OR in clauses and AND between clauses. Create list of clauses [[l11, l21, ...], ..., [ln1, ln2, ...]]
    Input: parser tree in cnf form
    Output: simplify parser tree as above format """
    def simplifyClause(ptree) :
        """Implicit OR in clauses, ex: [+,[+,a,b],c] = [a,b,c]
        Input: parser tree of clause in CNF form
        Output: list of literals in clause """
        if isinstance(ptree, str) :
            result  = [ptree]
        else :  # ptree  =  [OR, p1, p2]
            result  = simplifyClause(ptree[1]) + simplifyClause(ptree[2])
        return result

    if isinstance(ptree, str) :  # ptree = "P" or "-P"
        result = [ [ptree] ]
    else :     # ptree  =  [op, p1, p2]
        op = ptree[0]
        if op == OR :
            result = [ simplifyClause(ptree[1]) + simplifyClause(ptree[2]) ]
        else : # op == AND
            result = simplifyCNF(ptree[1]) + simplifyCNF(ptree[2])
    return result

def eliminateDuplicates(ptree):
    """Eliminate duplicated disjunctive literal (A or ~A): A + .. + A + ... = ... A ...
    Input: ptree  is in simple cnf
    Output: equivalent parse tree with duplicates disjunctive literal elimination
    """
    result = []
    for clause in ptree:
        clause = list(set(clause)) # python removing duplicates in lists
        result = result + [clause]
    return result

def eliminateTautologies(ptree):
    """Eliminate tautology (remove clause) that contains  [ ..., A, ..., -A,... ] := TRUE
    Input: ptree  is in simple cnf
    Output: equivalent parse tree with duplicates tautology clauses elimination
    """
    result = []
    for clause in ptree:
        # Editing elements in a list in python -> change very ~A := A
        tmp = [s.replace('~', '') for s in clause]
        if (len(clause) == len(set(tmp))): # no tautology
            result = result + [clause]         
    return result

def main(ptree):
    global cnf
    ptree1 = eliminateEquivalences(ptree)
    ptree1 = eliminateImplications(ptree1)
    #print "Elimination -> and <->: ", ptree1
    ptree2 = distributeNegations(ptree1)
    #print "Move ~ inside: ", ptree2

    #navieTseitin(ptree2); ptree3 = cnf + [["x" + str(count-1)]]
    #print ptree3
    
    ptree3 = tseitin(ptree2)
    print "CNF form parse tree: ", ptree3
    ptree3 = simplifyCNF(ptree3)
    #print "number of clause: ", len(ptree3)
    #print "Simple parser tree in CNF: ", ptree3
    
    ptree4 = eliminateDuplicates(ptree3)
    #print "Eliminate duplicates:", ptree4
    ptree5 = eliminateTautologies(ptree4)
    #print "Final CNF:", ptree5
    
    # remove duplicate negation ~~A = A
    for i, clause in enumerate(ptree5):
        for j, literal in enumerate(clause):
            clause[j] = literal.replace("~~", "")
    return ptree5
#main ()