#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 


# FormulaParser: From Boolean formula string -> Parse (syntax) tree

# Data structure
tokenList = []  # list symbols in input proposition formula
symbol = ""     # next symbol is placed into the parsing tree

# Operations
AND = "*"
OR  = "+"
NOT = "~"
IMPLY = "->"
EQUIV   = "<->"
BINARYOPS = (AND, OR, NOT, IMPLY, EQUIV)

LPAR = "("
RPAR = ")"
END  = "$"

# Separate string formula into list of symbols
def scan(formula):
    """Input:  proposition formula string
       Output: list of symbols"""
    # Pre-process string: input formula -> correct format (every symbols is separated by space)
    formula = formula.replace("(", " ( ")
    formula = formula.replace(")", " ) ")
    formula = formula.replace("*", " * ")
    formula = formula.replace("+", " + ")
    formula = formula.replace("~", " ~ ")
    formula = formula.replace("->", " -> ")
    formula = formula.replace("< ", "<")
    formula = formula.replace("<->", " <-> ")
    #print formula    
    symbols = []    
    for char in formula.split(): symbols.append(char)
    return symbols      

# Create syntax tree in the nested list form
def parse(symblist) :
    """Input: list of symbols from formula
       Output: parse tree in the format"""
    global tokenList, symbol
    tokenList = symblist
    getNext()
    ptree = parseE()
    if symbol != END :  # remain strange symbols
        print "Your formula is incorrect due to strange symbols:", symbol #, tokenList
    else:
        return ptree

def getNext() :
    """Delete the next symbol from the token list and copies it into "symbol" variable
       If tokenList is [], then  symbol = END """
    global tokenList, symbol
    if len(tokenList) == 0: symbol = END
    else :
        symbol    = tokenList[0]
        tokenList = tokenList[1:]

"""
Grammar rules of boolean logic expression:
    E := T op T | T
    T := -F | F
    F :=  p | ( E )
        where  op  is binary operation: and(*), or(+), imply(->) and equivalent(<->)
        p is a atomic proposition (alphabetic letter or string of letters)
"""
def parseE():
    """Create parse tree for grammar rule: E := T op T | T """
    tree1 = parseT()
    if symbol in BINARYOPS :  # T op T
        op = symbol
        getNext();
        tree2 = parseT()
        result = [op, tree1, tree2]
    else :  # T
        result = tree1
    return result
        
def parseT():
    """Create parse tree for grammar rule: T := -F | F """
    if symbol == NOT :  # - F
        getNext()
        tree = parseF()
        result = [NOT, tree]
    else :  # F
        result = parseF()
    return result
        
def parseF():
    """Create parse tree for grammar rule: F :=  p | ( E ) """
    if symbol.isalnum(): # p is combine of alpha and number
        result = symbol
        getNext()
    elif symbol == "(":  # ( E ) 
        getNext()
        result = parseE()
        if symbol == ")":
            getNext()
        else:
            print "Your formula is incorrect due to missing right parenthesis."
    else :
        result = []
        print """Your formula is incorrect due to  strange symbol.
        Please review your formula to make sure that your formula doesn't contain number or special characters"""
    return result

def main(formula) :
    """Input:  a proposition formula
       Output: a parse tree (syntax tree)"""
    #formula = raw_input("Your propositional formula: ")
    ptree   = parse(scan(formula))
    print "Parse tree: ", ptree
    return ptree
    
#main("((a+b)*((b+c)+d))+e")