#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 
import os

# CallMiniSat: run minisat from inside Python code
# By convention: file minisat2 in the parent of source code file, and data must be placed in folder data
def main(inputFile):
    fipath = "../data/" + inputFile + ".cnfinput"
    fopath = "../data/" + inputFile + ".cnfoutput"
    folog  = "../data/" + inputFile + ".minisatlog"
    
    #os.system("ls ~/Documents/")
    #os.system("../minisat2 ../data/sudoku01.cnfinput ../sudoku01.cnfoutput")
    print "******************************"
    os.system("../minisat2 " + fipath + " " + fopath)
    print "******************************"
    
#main("sudoku01")