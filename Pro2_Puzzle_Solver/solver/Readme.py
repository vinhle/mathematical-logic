# Print introduction about the program
def introduction():
    print """    Wellcome to JAIST Puzzle solver!
    Author: Le Vinh (1410212) - JAIST, 5/2015
    - The Puzzle solver is a program with command GUI style and it cooperates with SAT solver (miniSAT)
    - The Sudoku solver can solve puzzle in many sizes (n*m)
    - The program provides some functions, and please choose functions you like
        1. Try with default sample puzzle01
        2. Run with your input file (you also can run with other sample: puzzle02, puzzle03, ...)
        3. Help - Explain about the program.
        4. Exit program"""

# Print explanation about the program
def helpsolver():
    print """    1. Introduction about Puzzle game:
    - Puzzle is a grid n * m size
    - Original puzzle contains some cells that is filled with a number.
    The number means the number of black cells around it (this cell can also be black cell)
    - Answer puzzle is a filled grid with black and white cells
    2. File format convention
    - The raw input file: First line contains only two number that is the size of puzzle
    (Ex: 10*20 means the size is 10*20). The following lines are grid of puzzle, empty cell is denoted by .
    See example files for more detail ...
        + After the solver run, the following files will be generated automatically:
        + <file_name>.cnfinput file: DIMACS format (input file for miniSat solver)
        Read more: http://logic.pdmi.ras.ru/~basolver/dimacs.html
        + <file_name>.cnfoutput file: output file from miniSat solver
        + <file_name>.gridouput (final output): white cell is .. , black cell is ##,
          white number cell is "."+"number", black number cell is "#" + "number"
          (ex: .5 means the number is 5 and this cell is white
               #5 means the number is 5 and this cell is black)
        
    3. Run the program (In Linux enviroment)
    - Your data must be placed in folder data
    - Your computer must install Python 2.7
    - Open terminal
    - Move to program source: cd Puzzle/src/solver
    - Run by command: python ./PuzzleSolver.py
    """