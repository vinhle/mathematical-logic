#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 

from pprint import pprint

# PuzzleToSat: convert from DIMACS format to grid ouput of puzzle solution

# convert one dimensional array to two dimensional array python 
# convert list l to two-dimension list with n elements in each sub list
def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

def write_cnfoutput_to_grid(inputFile, row, col):    
    
    datapath = "../data/" + inputFile + ".cnfoutput"
    
    # array includes all propositional variable
    array = []
    result = None
    try:
        fi = open(datapath, "r")
        for line in fi:
            if result == None:
                result = line.split()[0] # only get the first element in line
                if result == "UNSAT":
                    print "There is no solution for this puzzle"
                    break
                else: continue# "SAT"                    
            else:
                #array.append(int(x) for x in line.split())
                for e in line.split():
                    if (e != "0"): array.append(int(e))
        #print array
    except IOError:
        print "The file does not exist."
    except AssertionError:
        print "Error size of grid soduku"
    #print array  
    
    # array includes all number in puzzle solution
    # Them nhung bien chua co trong qua trinh en-decode, nhung bien nay la flase het, tuong duong o trang
    if result == "SAT":
        for x in range(1, row*col+1):
            if (x not in array) & (-x not in array): array.append(-x)
    array = sorted(array, key=abs)
    #print array
    
    # Convert one-dimension array to two-dimension array
    grid = chunks(array, col)        
    print "Puzzle solution"
    # print solution to screen
    """
    formatno = '%2s'
    for row in grid:
        for cell in row:
            if cell > 0: print "## ",
            else: print ".. ",
            #print (formatno % str(cell)),
        print ""
    """
    #pprint(grid)
    
    # Write solution to file
    fo = file("../data/" + inputFile + ".gridoutput", "w")
    for row in grid:
        for cell in row:
            if cell > 0: fo.write("## ")
            else: fo.write(".. ")
        fo.write('\n')
    fo.close()
    
def main(inputFile, size):
    global row, col
    row = size[0]
    col = size[1]
    write_cnfoutput_to_grid(inputFile, row, col)

row = None # global variable for size of puzzle = maxrow * maxcolumn
col = None

#main("puzzle01", [10,10])