from pprint import pprint

def main(inputFile):
    # Input file
    fin = "../data/" + inputFile
    size = []
    gridin = []
    try:
        rawinput = open(fin, "r")        
        for line in rawinput:
            if len(size) == 0:
                for x in line.split(): # size is a list with 2 elements
                    size.append(int(x))
                continue
            gridin.append(line.split())
    except IOError:
        print "The file does not exist."
        return -1
    #pprint(gridin)
    
    # Output file
    fout = "../data/" + inputFile + ".gridoutput"
    gridout = []
    try:
        rawouput = open(fout, "r")        
        for line in rawouput:
            gridout.append(line.split())
    except IOError:
        print "The file does not exist."
        return -1
    #pprint(gridout)
    
    if len(gridout) > 0 : #there is a solution
        # Merge to display beauty in screen
        row = size[0]
        col = size[1]
        i = j = 0
        while i < row:
            while j < col:
                if gridin[i][j] == '.': print gridout[i][j],
                else: # gridin[i][j] is number
                    if gridout[i][j] == "..": # white cell
                        print "." + gridin[i][j],
                    else: print "#" + gridin[i][j],
                j += 1
            j = 0; i += 1;
            print
        
        # Merge to write to file
        fout2 = "../data/" + inputFile + ".gridoutput2"
        rawouput2 = open(fout2, "w")
        i = j = 0
        while i < row:
            while j < col:
                if gridin[i][j] == '.': rawouput2.write(gridout[i][j]+" ")
                else: # gridin[i][j] is number
                    if gridout[i][j] == "..": # white cell
                        rawouput2.write("." + gridin[i][j] + " ")
                    else:
                        rawouput2.write("#" + gridin[i][j] + " ")
                j += 1
            j = 0; i += 1;
            rawouput2.write("\n")
        
#main("puzzle02")