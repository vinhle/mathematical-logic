#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 

"""
Puzzle Solver - Main program
"""
import Readme
import PuzzleToSat
import CallMiniSat
import SatToPuzzle
import Display

def main():
    Readme.introduction()
    puzzleDefault = "puzzle01"
    size = None # size of puzzle
    
    while True:
        choice        = raw_input("* Your choice: ")
        
        if choice == "1":
            size = PuzzleToSat.main(puzzleDefault)
            CallMiniSat.main(puzzleDefault)
            SatToPuzzle.main(puzzleDefault, size)
            Display.main(puzzleDefault)
        else:
            if choice == "2":
                puzzleUser = raw_input("* Your file: ")
                size = PuzzleToSat.main(puzzleUser)
                CallMiniSat.main(puzzleUser)
                SatToPuzzle.main(puzzleUser, size)
                Display.main(puzzleUser)
            else:
                if choice == "3": Readme.helpsolver()
                else:
                    if choice == "4": print "Thank you, bye bye"; break
                    else: print "Your choice is incorrect format! Please input again."
        print "Finish"

main()