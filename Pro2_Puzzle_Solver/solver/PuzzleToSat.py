#--------------------------------------------------- 
#----- Le Vinh (1410212) - JAIST, 5/2015 -----------
#--------------------------------------------------- 

# PuzzleToSat: convert from puzzle problem in a raw input file into DIMACS format (input for miniSat solver)
from pprint import pprint
import itertools

size = [] # global variable for size of puzzle = maxrow * maxcolumn
def read_file(puzzlefile):
    """
    Read file puzzle problem in raw (grid) input file into a two dimensions array
    """
    global size     # access global variable
    size = []       # reset size after each testing time
    datapath = "../data/" + puzzlefile
    try:
        rawinput = open(datapath, "r")
        grid = []
        for line in rawinput:
            if len(size) == 0:
                for x in line.split(): # size is a list with 2 elements
                    size.append(int(x))
                continue
            grid.append(line.split())
    except IOError:
        print "The file does not exist."
        return -1
    except ValueError:
        print "There are strange symbols in your file. Please check again."
        return -1
    
    #pprint(grid)
    #print size
    return grid

def index(x, y):
    """ Return the index of variable (in array) of cell at row x, column y """
    maxcolumn = size[1]
    return x*maxcolumn + (y+1) #the index of variable >= 1

def card_constraint(k, varlist): # choose exactly k from n variables
    clause = []
    k = int(k)
    n = len(varlist) # the number of variables in list
    #print "%r %r" % (k,n)
    if k in range(1,n): # 1 =< k < 9
        # at least
        clause = list(itertools.combinations(varlist, n-k+1))
        # at most
        inverse_varlist = [-var for var in varlist]
        clause = clause + list(itertools.combinations(inverse_varlist, k+1))
        #print clause
        return clause
    
    if k == n:
        for i in varlist: clause.append([i])
        #print clause
        return clause
    if k == 0:
        for i in varlist: clause.append([-i])
        #print clause
        return clause

def puzzle_constraint(gridInput):
    """ Create the clauses from puzzle."""
    clauses = []
    
    row = size[0]-1 #index of list in python from 0
    col = size[1]-1
    step = size[1]
    x = 0; y = 0
    while x < len(gridInput):
        #print "row ", x
        while y < len(gridInput[x]):
            tmp_y = y; y += 1
            if gridInput[x][tmp_y].isdigit():
                var = index(x, tmp_y) # so thu tu cua bien
                #print "index %r %r " % (var,tmp_y)
                # cell number inside grid
                if (x in range(1,row)) & (tmp_y in range(1,col)): # range([start],[stop]) means start <= x < stop
                    
                    if int(gridInput[x][tmp_y]) > 9: print "The number in (%r,%r) > 9, then input file format error"%(x+1,tmp_y+1); return []                 
                    tmp = [var-step-1, var-step, var-step+1, var-1, var, var+1, var+step-1, var+step, var+step+1]
                    #print tmp
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    continue

                # cell number in first/last row/column
                if (x==0) & (tmp_y in range(1,col)):
                    if int(gridInput[x][tmp_y]) > 6: print "The number in (%r,%r) > 6, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var-1, var, var+1, var+step-1, var+step, var+step+1]
                    #print tmp
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    continue
                if (x==row) & (tmp_y in range(1,col)):
                    if int(gridInput[x][tmp_y]) > 6: print "The number in (%r,%r) > 6, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var-step-1, var-step, var-step+1, var-1, var, var+1]
                    #print tmp
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    continue
                if (tmp_y==0) & (x in range(1,row)):
                    if int(gridInput[x][tmp_y]) > 6: print "The number in (%r,%r) > 6, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var-step, var-step+1, var, var+1, var+step, var+step+1]
                    #print tmp
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    continue
                if (tmp_y==col) & (x in range(1,row)):
                    if int(gridInput[x][tmp_y]) > 6: print "The number in (%r,%r) > 6, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var-step-1, var-step, var-1, var, var+step-1, var+step]
                    #print tmp
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    continue
                
                # cell number in corner
                if (x==0) & (tmp_y==0):   # grid[0,0]
                    if int(gridInput[x][tmp_y]) > 4: print "The number in (%r,%r) > 4, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var, var+1, var+step, var+step+1]
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    #print tmp
                    continue
                if (x==0) & (tmp_y==col): # grid[0,n-1]
                    if int(gridInput[x][tmp_y]) > 4: print "The number in (%r,%r) > 4, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var-1, var, var+step-1, var+step]
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    #print tmp
                    continue
                if (x==row) & (tmp_y==0):   # grid[m-1,0]
                    if int(gridInput[x][tmp_y]) > 4: print "The number in (%r,%r) > 4, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var-step, var-step+1, var, var+1]
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    #print tmp
                    continue
                if (x==row) & (tmp_y==col): # grid[m-1,n-1]
                    if int(gridInput[x][tmp_y]) > 4: print "The number in (%r,%r) > 4, then input file format error"%(x+1,tmp_y+1); return []
                    tmp = [var-step-1, var-step, var-1, var]
                    clauses += card_constraint(gridInput[x][tmp_y], tmp)
                    #print tmp
                    continue
        y = 0; x += 1
    #pprint(clauses)            
    return clauses

def write_grid_to_cnfinput(gridInput, inputFile):
    """
    Write lists clause to DIMACS format (cnf format) input for SAT solver
    """
    clauses = puzzle_constraint(gridInput)
    print len(clauses)
    if len(clauses) > 0:
        #numvar = size[0]*size[1]
        flatten_clause1 = list(itertools.chain(*clauses))
        flatten_clause1 = [abs(var) for var in flatten_clause1]
        flatten_clause2 = set(flatten_clause1)
        #print flatten_clause2
        numvar = max(flatten_clause2)
        
        fo = file("../data/" + inputFile + ".cnfinput", 'w')
        fo.write('c Puzzle problem\n')
        fo.write('p cnf %d %d\n' % (numvar, len(clauses)))
        
        fo.write('\nc Puzzle constraints\n')
        for clause in clauses:
            for literal in clause:
                fo.write(str(literal) + ' ')
            fo.write('0\n')    
        fo.close()

def main(inputFile):
    gridInput = read_file(inputFile)
    if (gridInput != -1): # read file success
        print "Puzzle problem"
        write_grid_to_cnfinput(gridInput, inputFile)
        
        # print to screen input puzzle
        datapath = "../data/" + inputFile
        try:
            rawinput = open(datapath, "r")
            for line in rawinput: print line,
        except IOError:
            print "The file does not exist."
    print
    return size

#main("puzzle03")